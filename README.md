# hard to place deceased donor kidney app


# # Using this module

In order to use this python module, write your JSON string into the file fields.json and run h2p.py. It is possible to configure it to take input from STDIN, but in my testing all of the double quotes needed to be escaped in order for the string to be properly read in, and that is not ideal

# # How it works

The JSON is read in as a dict and assigned to an instance of the Donor class, which holds the necessary functions. These functions are run, and the output (kdpi and discard percent) is printed as a JSON string to STDOUT. Making this object oriented is a little overkill for the current functionality, but makes things easier to organize if expansion is desired in the future.

This module is only designed to work with complete input, if partial input is desired I would need to know what to use as a neutral input to any given field. That doesn't seem like it would yield valid output though.

If you want to use different strings as the labels for fields in the JSON, just change the corresponding strings at the top of donor.py.

The current model file was created using scikit-learn version 1.1.3, and may not work properly with other versions. Please remember to update this version number in requirements.txt if the model is updated in the future.

There is currently no logging or useful error handling implemented, the module is small enough that it doesn't seem very necessary to me right now.
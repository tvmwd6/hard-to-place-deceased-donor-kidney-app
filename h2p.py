from donor import Donor
import json


VERSION = "1.0"

def main():
    with open("fields.json", "r") as infile:
        data = json.load(infile)
    d = Donor(data)
    print(json.dumps({"kdpi":d.kdpi(), "discard":d.classify()}))

if __name__ == "__main__":
    main()
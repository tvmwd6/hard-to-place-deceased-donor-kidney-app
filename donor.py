import pandas as pd
import numpy as np
import math
import pickle


# This constant is updated occasionally, put the new value here and save the file.
KDPI_RATIO = 1.318253823684

# What the current model file is called
MODEL_FILE_NAME = "H2P.pkl"

# These are the labels that we expect to receive in json as keys, change as needed
GENDER_STRING =             "gender"
GLOM_COUNT_STRING =         "glom_count"
CAUSE_OF_DEATH_STRING =     "cod"
NON_HRT_STRING =            "nhr"
CREATININE_STRING =         "creat"
HEPC_STRING =               "hcv"
DIABETES_STRING =           "diab"
GLOMULOSCLEROSIS_STRING =   "glom_scler"
PUMP_STRING =               "pump"
FIBROSIS_STRING =           "fib"
VASCULAR_STRING =           "vasc"
HYPERTENSION_STRING =       "hyper"
AGE_STRING =                "age"
RACE_STRING =               "race"
HEIGHT_STRING =             "height"
WEIGHT_STRING =             "weight"
CLAMP_STRING =              "clamp"
CIT_STRING =                "cit"


class Donor :
    def __init__(self, data : dict):
        # relies on consistent order of dict elements, so python 3.7+ is required
        self.data = data
        
    def __kdri_to_kdpi(self, n):
        """
        Converts kdri to kdpi
        """
        kdpi=n

        if n >0.0000 and n<=0.49288618187448: kdpi=0
        elif n>0.49288618187448 and n<=0.55766759610873: kdpi=1
        elif n>0.55766759610873 and n<=0.57704916969103: kdpi=2
        elif n>0.57704916969103 and n<=0.59364308928216: kdpi=3
        elif n>0.59364308928216 and n<=0.60609593309627: kdpi=4

        elif n>0.60609593309627 and n<=0.61821968953949: kdpi=5
        elif n>0.61821968953949 and n<=0.62983877817678: kdpi=6
        elif n>0.62983877817678 and n<=0.63920695742794: kdpi=7
        elif n>0.63920695742794 and n<=0.64902957059053: kdpi=8
        elif n>0.64902957059053 and n<=0.65809432951739: kdpi=9

        elif n>0.65809432951739 and n<=0.66612562506720: kdpi=10
        elif n>0.66612562506720 and n<=0.67558452491476: kdpi=11
        elif n>0.67558452491476 and n<=0.68412337829430: kdpi=12
        elif n>0.68412337829430 and n<=0.69185602594588: kdpi=13
        elif n>0.69185602594588 and n<=0.69880621985069: kdpi=14

        elif n>0.69880621985069 and n<=0.70790554868322: kdpi=15
        elif n>0.70790554868322 and n<=0.71579819664996: kdpi=16
        elif n>0.71579819664996 and n<=0.72404263514936: kdpi=17
        elif n>0.72404263514936 and n<=0.73190408867748: kdpi=18
        elif n>0.73190408867748 and n<=0.74144299374481: kdpi=19

        elif n>0.74144299374481 and n<=0.74970386281724: kdpi=20
        elif n>0.74970386281724 and n<=0.75737785660771: kdpi=21
        elif n>0.75737785660771 and n<=0.76631975317226: kdpi=22
        elif n>0.76631975317226 and n<=0.77361805204334: kdpi=23
        elif n>0.78299638489056 and n<=0.78264427231080: kdpi=24

        elif n>0.78264427231080 and n<=0.79070589237185: kdpi=25
        elif n>0.79070589237185 and n<=0.79775240427962: kdpi=26
        elif n>0.79775240427962 and n<=0.80533011892674: kdpi=27
        elif n>0.80533011892674 and n<=0.81290014488090: kdpi=28
        elif n>0.81290014488090 and n<=0.82079901878607: kdpi=29

        elif n>0.82079901878607 and n<=0.82783534753742: kdpi=30
        elif n>0.82783534753742 and n<=0.83590776577662: kdpi=31
        elif n>0.83590776577662 and n<=0.84241964344697: kdpi=32
        elif n>0.84241964344697 and n<=0.85068701756456: kdpi=33
        elif n>0.85068701756456 and n<=0.85855116695904: kdpi=34

        elif n>0.85855116695904 and n<=0.86674973513388: kdpi=35
        elif n>0.86674973513388 and n<=0.87531645075402: kdpi=36
        elif n>0.87531645075402 and n<=0.88387732375932: kdpi=37
        elif n>0.88387732375932 and n<=0.89239448179147: kdpi=38
        elif n>0.89239448179147 and n<=0.90094830333877: kdpi=39

        elif n>0.90094830333877 and n<=0.91013985906309: kdpi=40
        elif n>0.91013985906309 and n<=0.91862526835859: kdpi=41
        elif n>0.91862526835859 and n<=0.92683752357503: kdpi=42
        elif n>0.92683752357503 and n<=0.93686414179677: kdpi=43
        elif n>0.93686414179677 and n<=0.94645959319719: kdpi=44

        elif n>0.94645959319719 and n<=0.95388992193620: kdpi=45
        elif n>0.95388992193620 and n<=0.96357341294590: kdpi=46
        elif n>0.96357341294590 and n<=0.97223571380888: kdpi=47
        elif n>0.97223571380888 and n<=0.98143155774226: kdpi=48
        elif n>0.98143155774226 and n<=0.99161832376591: kdpi=49

        elif n>0.99161832376591 and n<=1.00000000000000: kdpi=50
        elif n>1.00000000000000 and n<=1.01019461458336: kdpi=51
        elif n>1.01019461458336 and n<=1.02114342056441: kdpi=52
        elif n>1.02114342056441 and n<=1.03235541277888: kdpi=53
        elif n>1.03235541277888 and n<=1.04108037909272: kdpi=54

        elif n>1.04108037909272 and n<=1.05134994466249: kdpi=55
        elif n>1.05134994466249 and n<=1.06124526827265: kdpi=56
        elif n>1.06124526827265 and n<=1.07289757239163: kdpi=57
        elif n>1.07289757239163 and n<=1.08168823370293: kdpi=58
        elif n>1.08168823370293 and n<=1.09378373320070: kdpi=59

        elif n>1.09378373320070 and n<=1.10488692564162: kdpi=60
        elif n>1.10488692564162 and n<=1.11276621409158: kdpi=61
        elif n>1.11276621409158 and n<=1.12617056845307: kdpi=62
        elif n>1.12617056845307 and n<=1.13787330701880: kdpi=63
        elif n>1.13787330701880 and n<=1.15032326169124: kdpi=64

        elif n>1.15032326169124 and n<=1.16220957579890: kdpi=65
        elif n>1.16220957579890 and n<=1.17507855419745: kdpi=66
        elif n>1.17507855419745 and n<=1.18902851528348: kdpi=67
        elif n>1.18902851528348 and n<=1.20326536655901: kdpi=68
        elif n>1.20326536655901 and n<=1.21549694329305: kdpi=69

        elif n>1.21549694329305 and n<=1.22745509802558: kdpi=70
        elif n>1.22745509802558 and n<=1.24150331981455: kdpi=71
        elif n>1.24150331981455 and n<=1.25477089231939: kdpi=72
        elif n>1.25477089231939 and n<=1.26711542725798: kdpi=73
        elif n>1.26711542725798 and n<=1.28049023409929: kdpi=74

        elif n>1.28049023409929 and n<=1.29521020345262: kdpi=75
        elif n>1.29521020345262 and n<=1.30795519779436: kdpi=76
        elif n>1.30795519779436 and n<=1.32331056421167: kdpi=77
        elif n>1.32331056421167 and n<=1.33954901319506: kdpi=78
        elif n>1.33954901319506 and n<=1.354173086799372: kdpi=79

        elif n>1.35417308679937 and n<=1.37299460853031: kdpi=80
        elif n>1.37299460853031 and n<=1.39325536754786: kdpi=81
        elif n>1.39325536754786 and n<=1.41385921859655: kdpi=82
        elif n>1.41385921859655 and n<=1.43186673205109: kdpi=83
        elif n>1.43186673205109 and n<=1.44779686858670: kdpi=84

        elif n>1.44779686858670 and n<=1.46753663431589: kdpi=85
        elif n>1.46753663431589 and n<=1.48887827371017: kdpi=86
        elif n>1.48887827371017 and n<=1.51131328801805: kdpi=87
        elif n>1.51131328801805 and n<=1.53637078180307: kdpi=88
        elif n>1.53637078180307 and n<=1.56599436621390: kdpi=89

        elif n>1.56599436621390 and n<=1.59388080454631: kdpi=90
        elif n>1.59388080454631 and n<=1.62599998190581: kdpi=91
        elif n>1.62599998190581 and n<=1.66067309649100: kdpi=92
        elif n>1.66067309649100 and n<=1.69776893848280: kdpi=93
        elif n>1.69776893848280 and n<=1.74595729786717: kdpi=94

        elif n>1.74595729786717 and n<=1.80117569936833: kdpi=95
        elif n>1.80117569936833 and n<=1.86909994095062: kdpi=96
        elif n>1.86909994095062 and n<=1.94997197367416: kdpi=97
        elif n>1.94997197367416 and n<=2.06166972375206: kdpi=98
        elif n>2.06166972375206 and n<=2.23606163552925: kdpi=99

        elif n>2.23606163552925 and n<=3.32515071199901: kdpi=100
        elif n>3.32515071199901 and n<=999999999.00000000000000: kdpi=100

        return kdpi/100

    def kdpi(self):
        """
        Does the calculations to determine kdpi based on this donor's data
        """
        FP= 0.0128*(self.data[AGE_STRING]-40)-0.0194*(self.data[AGE_STRING]-18)*int(self.data[AGE_STRING]<18)+0.0107*(self.data[AGE_STRING]-50)*int(self.data[AGE_STRING]>50)
        SP= -0.0464*((self.data[HEIGHT_STRING]-170)/10)-0.0199*((self.data[WEIGHT_STRING]-80)/5)*int(self.data[WEIGHT_STRING]<80)
        TP= 0.179*int(self.data[RACE_STRING]==2) + 0.1260*int(self.data[HYPERTENSION_STRING]!=1 and self.data[HYPERTENSION_STRING]!=998)+0.1300*int(self.data[DIABETES_STRING]!=1 and self.data[DIABETES_STRING]!=998)
        F4P= 0.0881*int(self.data[CAUSE_OF_DEATH_STRING]==2)+0.2200*(self.data[CREATININE_STRING]-1)-0.2090*(self.data[CREATININE_STRING]-1.5)*int(self.data[CREATININE_STRING]>1.5)
        F5P= 0.2400*int(self.data[HEPC_STRING]==3) + 0.1330*int(self.data[NON_HRT_STRING]==2)
        X_score = FP+SP+TP+F4P+F5P
        KDRI_rao=math.exp(X_score)
        KDRI_med=KDRI_rao/KDPI_RATIO
        kdpi = self.__kdri_to_kdpi(KDRI_med)
        return kdpi
    
    def classify(self):
        """
        Use the data for this donor to classify it using the model.
        
        Requires:
            current pickled model file in same dir and named H2P.pkl

        Output:
            discard rate for this donor
        """
        model = pickle.load(open(MODEL_FILE_NAME,"rb"))
        classification = pd.DataFrame(np.round(model.predict_proba(np.transpose(pd.DataFrame(list(self.data.values())[:-1]))),2))
        discard = pd.DataFrame(classification).iat[0,0]*100
        if self.data[CIT_STRING] == 1: discard *= 1.05
        elif self.data[CIT_STRING] == 2: discard *= 1.15
        elif self.data[CIT_STRING] == 3: discard *= 1.30
        elif self.data[CIT_STRING] == 4: discard *= 1.50
        elif self.data[CIT_STRING] == 5: discard *= 1.75
        if discard > 100: discard = 100
        discard=np.round(discard,2)
        return discard
